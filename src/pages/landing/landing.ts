import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-landing',
	templateUrl: 'landing.html',
})
export class LandingPage {
	currentIndex:any;
	slidertab:any;
	slide1 = false;
	slide2 = true;
	@ViewChild('mySlider') slider: Slides;
	@ViewChild(Slides) slides: Slides;
  	constructor(public navCtrl: NavController, public navParams: NavParams) {
  	}

	ionViewDidLoad() {
	    console.log('ionViewDidLoad LandingPage');
	}
	toLogin(){
     	this.navCtrl.push('LoginPage');
  	}
  	goToSlide(id) {
	    this.slider.slideTo(id, 1);
	}
	next() {
	  	let currentIndex = this.slider.getActiveIndex();
	  	this.slidertab = currentIndex;
	  	console.log("Current Slide No:", currentIndex);
	  	if(currentIndex == 0){
	    	// this.slide1 = !this.slide1;
	    	this.slide1 = false;
	    	this.slide2 = true;
	    }
	    if(currentIndex == 1){
	    	this.slide1 = true;
	    	this.slide2 = false;
	    }
  	}


}
