import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupdetailPage } from './signupdetail';

@NgModule({
  declarations: [
    SignupdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupdetailPage),
  ],
})
export class SignupdetailPageModule {}
