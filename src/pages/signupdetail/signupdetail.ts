import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
    selector: 'page-signupdetail',
    templateUrl: 'signupdetail.html',
})
export class SignupdetailPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SignupdetailPage');
    }
    gotoSignup() {
        this.navCtrl.push('SignupPage');  
    }
    gotoWelcome() {
        this.navCtrl.push('WelcomePage');
    }

}
