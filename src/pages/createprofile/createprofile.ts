import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

@IonicPage()
@Component({
  	selector: 'page-createprofile',
  	templateUrl: 'createprofile.html',
})
export class CreateprofilePage {

	currentIndex:any;
	slidertab:any;
	@ViewChild('mySlider') slider: Slides;
	@ViewChild(Slides) slides: Slides;

  	constructor(public navCtrl: NavController, public navParams: NavParams) {
  		let id = this.navParams.get("id");
	  	this.slidertab = id;
	  	console.log("id", id);
	  	setTimeout(() => {
	    	this.goToSlide(id);
	  	}, 500);
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad CreateprofilePage');
  	}
  	gotoWelcome() {
        this.navCtrl.push('WelcomePage');
    }
    goToSlide(id) {
    	// this.slider.slideTo(id, 500);
  	}
  	next() {
  		let currentIndex = this.slider.getActiveIndex();
  		this.slidertab = currentIndex;
  		console.log("Current Slide No:", currentIndex);
  	}

}
